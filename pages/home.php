<?php
if (!defined('SECURE_CONSTANT')){ die('Your don\'t have permission to view this page'); }

include_once 'templates/header.html';

$news = General::loadArticles(CURRENT_LANGUAGE);
?>

<div class="container main-section">
<?php 
	foreach ($news as $article){
		echo "
		<div class='row'>
			<div class='col s12'>
				<div class='card blue darken-1'>
					<div class='card-content white-text'>
						<span class='card-title'>". $article['Title'] ."</span>
						<p>". $article['Content'] ."</p>
					</div>
				</div>
			</div>
		</div>
		";
	}
?>
</div>

<?php
include_once 'templates/footer.html';
?>