<?php
//System constants
define("SECURE_CONSTANT", "Simple Site");
define("PAGE_IDENTIFIER", "page");
define("LANGUAGE_IDENTIFIER", "language");
define("CURRENT_LANGUAGE", isset($_GET[LANGUAGE_IDENTIFIER]) ? $_GET[LANGUAGE_IDENTIFIER] : 'EN');

//Requirements
require 'classes/General.php';
require 'classes/Database.php';

//Page loader
General::router();
?>