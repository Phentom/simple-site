<?php
if (!defined('SECURE_CONSTANT')){ die('Your don\'t have permission to view this page'); }

/**
 * Holds the news information with the diferent languages
 */

$articles = array(
	'EN' => array(
		array(
			'Title' => 'Welcome',
			'Content' => 'Welcome to this simple website!<br/>
						  Enjoy coding!',
			'Date' => '19/09/2016',
		),
	),
	'PT' => array(
		array(
			'Title' => 'Bem vindo',
			'Content' => 'Bem vindo a este simple site!<br/>
						  Diverte-te a programar!',
			'Date' => '19/09/2016',
		),
	)
);
?>