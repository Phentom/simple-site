<?php
if (!defined('SECURE_CONSTANT')){ die('Your don\'t have permission to view this page'); }

/**
 * Holds the language tokens for the diferent languages
 */

$language_tokens = array(
	'EN' => array(
		'##WELCOME##' => 'Welcome!',
		'##LOGIN##' => 'Login',
		'##REGISTER##' => 'Register',
	),
	'PT' => array(
		'##WELCOME##' => 'Bem Vindo!',
		'##LOGIN##' => 'Entrar',
		'##REGISTER##' => 'Registar',
	)
);
?>