# README #

This is a very simple website for people to develop with.
Use as you like!
Have fun coding!

### Info ###

* Version 1.0
* Just contact if you need some help or commit to make it better!

### Requirements ###

* PHP 5.x.x
* MySQL (if you want to save and retrieve data)

### Features ###

* News page
* Language tokens
* Database helper
* Router

### What can i do? ###

* You can develop anything you want with this!
* Add your classes, functions, views, styles, ideas!