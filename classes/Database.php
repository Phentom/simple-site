<?php
/**
 * Collection of database functionalities
 * 
 * @package Simple Site
 * @subpackage Classes
 * @author Phentom
 * @version 1.0
 */
class Database{
	/**
	 * Database hostname
	 * @var string
	 */
	private static $db_host = "localhost";
	
	/**
	 * Database username
	 * @var string
	 */
	private static $db_user = "root";
	
	/**
	 * Database password
	 * @var string
	 */
	private static $db_pass = "";
	
	/**
	 * Database name
	 * @var string
	 */
	private static $db_name = "mysql";
	
	/**
	 * Database port
	 * @var string
	 */
	private static $db_port = "3306";
	
	/**
	 * Performes a query to the database and returns the result
	 * 
	 * @param string $query
	 */
	public static function query($query){
		$con = new mysqli(self::$db_host, self::$db_user, self::$db_pass, self::$db_name, self::$db_port);
		
		if ($con->errno){
			self::error($con);
		}
		
		$result = $con->query($query);
		
		if ($con->errno){
			self::error($con);
		}
		
		$con->close();
		
		if ($result !== TRUE){
			$values = array();
			
			while ($fields = $result->fetch_array(MYSQLI_ASSOC)){
				$values[] = $fields;
			}
			
			return $values;
		}
		else{
			return $result;
		}
	}
	
	public static function login($user, $password){
		//Your login code here
	}
	
	public static function register($user, $password, $email){
		//Your register code here
	}
	
	/**
	 * Displays the database error and message
	 * 
	 * @param object $con
	 */
	private static function error($con){
		ob_clean();
		echo "Code: ". $con->errno ."<br/>";
		echo "Message: ". $con->error;
		die;
	}
}
?>