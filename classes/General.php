<?php
/**
 * Collection of general functionalities
 * 
 * @package Simple Site
 * @subpackage Classes
 * @author Phentom
 * @version 1.0
 */
class General{
	
	/**
	 * Loads the page according to the page identifier
	 */
	public static function router(){
		$page = (isset($_GET[PAGE_IDENTIFIER]) ? $_GET[PAGE_IDENTIFIER] : '');
		
		$tokens = General::loadLanguageTokens(CURRENT_LANGUAGE);
		
		if (empty($page)){
			include_once 'pages/home.php';
		}
		elseif (is_readable('pages/'. $page .'.php')){
			include_once 'pages/'. $page .'.php';
		}
		else{
			//Your custom error page
		}
	}
	
	/**
	 * Returns the array of articles for a given language
	 * 
	 * @param string $language
	 * @return array
	 */
	public static function loadArticles($language = 'EN'){
		include 'includes/news.php';
		
		return $articles[$language];
	}
	
	/**
	 * Returns the array of tokens for a given language
	 *
	 * @param string $language
	 * @return array
	 */
	public static function loadLanguageTokens($language = 'EN'){
		include 'includes/tokens.php';
	
		return $language_tokens[$language];
	}
}
?>